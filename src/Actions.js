export function ADD_CURRENT_ADDRESS(payload) {
  return {
    type: 'ADD_CURRENT_ADDRESS',
    payload: payload
  }
}
export function ADD_CURRENT_TRANS(payload) {
    return {
        type: 'ADD_CURRENT_TRANS',
        payload: payload
    }
}
export function ADD_CURRENT_ADDRESS_TRANS(payload) {
    return {
        type: 'ADD_CURRENT_ADDRESS_TRANS',
        payload: payload
    }
}
export function ADD_CURRENT_ADDRESS_ANALYTIC(payload) {
  return {
    type: 'ADD_CURRENT_ADDRESS_ANALYTIC',
    payload: payload
  }
}
