import Web3 from 'web3';

const web3 = new Web3(global.RPC);
export default web3;
