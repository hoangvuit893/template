import React, { Component } from 'react';

import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  Button,
  Modal, ModalHeader, ModalBody, ModalFooter,
  Form, Input
} from 'reactstrap';
import 'moment-timezone';
import validateInput from '../../shared/validations/browserLink';
import TextArea from "../Common/TextArea";
import TextFieldGroup from "../Common/TextFieldGroup";
let headers = new Headers();
const auth = localStorage.getItem('auth');
headers.append('Authorization', 'Bearer ' + auth);

class Cards extends Component {
  constructor(props) {
    super(props);
    this.state = {
        data: [],
        categories: [],
        addresses: [],
        title: '',
        image: '',
        description: '',
        category: '',
        url: '',
        isActive100: false,
        isLoading: false,
        errors: {},
        action: '',
    };
  }
  async componentDidMount() {
    // const fetchData = {
    //   method: 'GET',
    //   headers: headers
    // };

    //   fetch(global.BASE_URL + '/admin/browser-links', fetchData).then( cards => {
    //       cards.json().then(result => {
    //           this.setState({
    //               data: result.data
    //           });
    //       })
    //   }).catch(console.log);
    // fetch(global.BASE_URL + '/admin/browser-link-categories', fetchData).then( cards => {
    //   cards.json().then(result => {
    //     this.setState({
    //         categories: result.data
    //     });
    //   })
    // }).catch(console.log);
  }
  toggle(action) {
    this.setState({
      modal: !this.state.modal,
      action,
      isLoading: false,
      updateId:'',
      title: '',
      image: '',
      category: '',
        url: '',
      isHide: false,
      isActive100: false,
      errors: {}
    });
  }
  execUpdate = (item) => {
      this.setState({
          action: 'update',
          updateId: item._id,
          title: item.title ? JSON.stringify(item.title,  null, '\t') : '',
          image: item.image,
          addresses: item.addresses ? item.addresses.join('\n') : '',
          description: item.description ? JSON.stringify(item.description,  null, '\t') : '',
          isActive100: item.isActive100,
          category: item.category._id,
          url: item.url,
          modal: !this.state.modal,
      });
  }
  cancelCreate() {
    this.setState({
      modal: false,
    });
  }
  updateApp= () => {
    if(this.isValid()) {
      this.setState({ errors: {}, isLoading: true });
      const id = this.state.updateId;
      // process utc time
      // end process utc time
      const body = {
                      title: this.state.title.trim() ? JSON.parse(this.state.title) : {},
                      image: this.state.image,
                      description: this.state.description.trim() ? JSON.parse(this.state.description) : {},
                      addresses: this.state.addresses.trim() ? this.state.addresses.toLowerCase().split('\n') : [],
                      language: this.state.title ? Object.keys(JSON.parse(this.state.title)) : [],
                      isActive100: this.state.isActive100,
                        category: this.state.category,
                      url: this.state.url,
                  };
      if (this.state.action === 'update') {
        const fetchData = {
          method: 'PUT',
          headers: headers,
          body: JSON.stringify(body)
        };
        fetch(global.BASE_URL + '/browser-link/'+id, fetchData).then(async () => {
          this.cancelCreate();
          this.componentDidMount();
          this.setState({created: 'Link updated successfully', isLoading: false, deleted: ''});
        }).catch(console.log);
      } else {
        const fetchData = {
          method: 'POST',
          headers: headers,
          body: JSON.stringify(body)
        };
        fetch(global.BASE_URL + '/admin/browser-links/', fetchData).then(async () => {
          this.cancelCreate();
          this.componentDidMount();
          this.setState({created: 'Link created successfully', isLoading: false, deleted: ''});
        }).catch(console.log);
      }
    }
  }
  isValid() {
    const {errors, isValid } = validateInput(this.state);
    if(!isValid) {
      this.setState({errors});
    }
    return isValid;
  }

    onChangeAct(e) {
        const checked = e.target.checked;
        this.setState({[e.target.name]: checked});
    }
    inputChange(e) {
      this.setState({ [e.target.name]: e.target.value });
    }
    onChangeCat(e) {
        this.setState({category: e.target.value});
    }
  deleteCard = id => {
    if (window.confirm ('Are you sure to delete this item?')) {
      const fetchData = {
        method: 'DELETE',
        headers: headers
      };
      fetch(global.BASE_URL + '/browser-link/' + id, fetchData).then(() => {
        this.setState({deleted: 'Item deleted', created: ''});
        // reload list
          this.componentDidMount()
        // const fetchData = {
        //   method: 'GET',
        //   headers: headers
        // };
        // const url = global.BASE_URL + '/admin/browser-links';
        // fetch(url, fetchData).then(async result => {
        //   const data = await result.json();
        //   this.state.data = data;
        //   this.setState(
        //     this.state
        //   );
        // }).catch(console.log);
      }).catch(console.log);
    }
  }
  render() {
    const {isActive100, errors, addresses, title, description, image, url, categories, category} = this.state;
    if(!this.state.isLoading) {
      return (
        <div className="animated fadeIn">
          <Row>
            <Col>
              <p style={styles.success}>{this.state.created}</p>
              <p style={styles.danger}>{this.state.deleted}</p>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> Type
                  <Button outline color="primary" style={styles.floatRight} size="sm"  onClick={e=>this.toggle('new')}>Add</Button>
                </CardHeader>
                <CardBody>
                  <Table responsive>
                    <thead>
                    <tr>
                      <th style={styles.wh40}>No.</th>
                      <th style={styles.wh40}>Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                      this.state.data.map((item, i) => {
                          return (
                            <tr key={i} style={styles.row}>
                              <td style={styles.w40}>{ i+1}</td>
                              <td style={styles.w40}><img src={item.name} alt="" width="50"/></td>
                              <td style={styles.w15}>
                                  <Button outline color="primary" size="sm" onClick={(e) => this.execUpdate(item) }>Update</Button>&nbsp;
                                  <Button outline color="danger" size="sm" onClick={(e) => this.deleteCard(item._id) }>Delete</Button>
                              </td>
                            </tr>
                          );
                      })
                    }
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Modal isOpen={this.state.modal} toggle={e=>this.cancelCreate()} className={this.props.className}>
              <ModalHeader toggle={()=>this.toggle()}>{this.state.action === 'update'? 'Edit' : 'Create '}</ModalHeader>
              <ModalBody>
                <Form>
                    <TextArea
                        field="name"
                        label="Name"
                        value={title}
                        error={errors.title}
                        onChange={e=>this.inputChange(e)}
                        rows="5"
                    />
                </Form>
              </ModalBody>
              <ModalFooter>
                <Button color="primary" onClick={e=>this.updateApp()} disabled={this.state.isLoading}>Save</Button>{' '}
                <Button color="secondary" onClick={e=>this.cancelCreate()}>Cancel</Button>
              </ModalFooter>
            </Modal>
        </div>
      );
    }
    return (
      <div id="page-loading">
        <div className="three-balls">
          <div className="ball ball1"></div>
          <div className="ball ball2"></div>
          <div className="ball ball3"></div>
        </div>
      </div>
    );
  }
}

const styles = {
    a : {
        textDecoration: 'none'
    },
    floatRight: {
      float: "right"
    },
    spinner: {
      width: "30px"
    },
    center: {
      textAlign: "center"
    },
    tbody: {
      height: "380px",
      overflowY: "auto"
    },
    wh5: {
      width: "5%",
      float: "left",
      height: "70px"
    },
    wh10: {
      width: "9.8%",
      float: "left",
      height: "70px"
    },
    wh15: {
      width: "15%",
      float: "left",
      height: "70px"
    },
    wh20: {
        width: "20%",
        float: "left",
        height: "70px"
    },
    wh25: {
      width: "25%",
      float: "left",
      height: "70px"
    },
    wh30: {
      width: "30%",
      float: "left",
      height: "70px"
    },
    wh40: {
      width: "40%",
      float: "left",
      height: "70px"
    },
    w5: {
      width: "5%",
      float: "left",
    },
    w10: {
      width: "10%",
      float: "left",
    },
    w15: {
      width: "15%",
      float: "left",
    },
    w20: {
        width: "20%",
        float: "left",
    },
    w25: {
      width: "25%",
      float: "left",
    },
    w30: {
      width: "30%",
      float: "left",
    },
    w40: {
      width: "40%",
      float: "left",
    },
    row: {
      float: "left",
      width: "100%"
    },
    success: {
      color: 'green'
    },
    danger: {
      color: 'red'
    },
    mgl5: {
      marginLeft: '5px'
    }
}

export default Cards;
