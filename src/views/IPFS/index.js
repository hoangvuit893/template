import React, { Component } from 'react';

import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    Table,
    Button, Input
} from 'reactstrap';
import 'moment-timezone';
import axios from 'axios';
class Ipfs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            link: '',
            isLoading: false
        };
    }
    async componentDidMount() {

    }
    goSearch() {
        const link = this.state.link;
        const dir = link.substr(link.indexOf('#/files') + '#/files'.length);
        this.setState({isLoading: true});
        const authUser = process.env.REACT_APP_IPFS_AUTH_USER;
        const pass = process.env.REACT_APP_IPFS_AUTH_PWD;
        const ipfsApi= process.env.REACT_APP_IPFS_API;
            fetch(global.BASE_URL + '/proxy-api?method=POST&url=' + encodeURIComponent('https://'+authUser+':'+pass+'@'+ipfsApi+'/api/v0/files/stat?arg='+encodeURIComponent(dir)+'&stream-channels=true')).then(async res => {
            res = await res.json();
                axios.get(global.BASE_URL + '/proxy-api?method=POST&url=' + encodeURIComponent('https://'+authUser+':'+pass + '@'+ipfsApi+'/api/v0/ls?arg=' + res.Hash)).then(res => {
                const items = res.data.Objects[0].Links || [];
                const data = [];
                for (let v of items) {
                    if (v.Type === 2)
                        data.push({hash: v.Hash, name: v.Name});
                }
                this.setState({data, isLoading: false});
            }).catch(() => {
                this.setState({isLoading: false});
            });
        });

    }
    inputChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    render() {
        const {data, link} = this.state;
        if(!this.state.isLoading) {
            return (
                <div className="animated fadeIn">
                    <Row>
                        <Col>
                            <p style={styles.success}>{this.state.created}</p>
                            <p style={styles.danger}>{this.state.deleted}</p>
                            <Card>
                                <CardHeader>
                                    <i className="fa fa-align-justify"></i> IPFS TOOL
                                    <div style={styles.tags}>
                                        <div>
                                            <Input style={styles.searchInput} onChange={(e) => this.inputChange(e)} name="link" value={link}/>
                                            <Button outline color="success" style={styles.floatRight} size="sm"  onClick={e=>this.goSearch()}>Search</Button>
                                        </div>
                                    </div>
                                </CardHeader>
                                <CardBody>
                                    <Table responsive>
                                        <thead>
                                        <tr>
                                            <th>Image (Total: {data.length})</th>
                                            {/*<th style={styles.wh15}>Screen Name</th>*/}
                                            {/*<th style={styles.wh15}>Name</th>*/}
                                            {/*<th style={styles.wh10}>Image</th>*/}
                                            {/*<th style={styles.wh10}>Votes</th>*/}
                                            {/*<th style={styles.wh10}>Profile</th>*/}
                                            {/*<th style={styles.wh10}>Hidden</th>*/}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            data.map((item, i) => {
                                                return (
                                                    <tr key={i} style={styles.row}>
                                                        <td><a href={process.env.REACT_APP_IPFSGW_URL + '/ipfs/' + item.hash + '?filename=' +item.name} target="_blank">{process.env.REACT_APP_IPFSGW_URL + '/ipfs/' + item.hash + '?filename=' +item.name}</a></td>
                                                        {/*<td style={styles.w15}>{item.screen_name}</td>*/}
                                                        {/*<td style={styles.w15}>{item.name}</td>*/}
                                                        {/*<td style={styles.w10}><a href={item.profile_image_url} target="_blank">View</a></td>*/}
                                                        {/*<td style={styles.w10}>*/}
                                                            {/*{item.votes}*/}
                                                        {/*</td>*/}
                                                        {/*<td style={styles.wh10}><a href={"https://twitter.com/"+item.screen_name} target="_blank">View</a></td>*/}
                                                        {/*<td style={styles.w10}>*/}
                                                            {/*<Input key={no} type="checkbox" value={item.id} onChange={()=>this.onChangeBlock(item.id, !item.blocked, i)} checked={item.blocked||false}/>*/}
                                                        {/*</td>*/}
                                                    </tr>
                                                );
                                            })
                                        }
                                        </tbody>
                                    </Table>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>
            );
        }
        return (
            <div id="page-loading">
                <div className="three-balls">
                    <div className="ball ball1"></div>
                    <div className="ball ball2"></div>
                    <div className="ball ball3"></div>
                </div>
            </div>
        );
    }
}

const styles = {
    a : {
        textDecoration: 'none'
    },
    floatRight: {
        float: "right"
    },
    spinner: {
        width: "30px"
    },
    center: {
        textAlign: "center"
    },
    tbody: {
        height: "380px",
        overflowY: "auto"
    },
    wh5: {
        width: "5%",
        float: "left",
        height: "70px"
    },
    wh10: {
        width: "9.8%",
        float: "left",
        height: "70px"
    },
    wh15: {
        width: "15%",
        float: "left",
        height: "70px"
    },
    wh20: {
        width: "20%",
        float: "left",
        height: "70px"
    },
    wh25: {
        width: "25%",
        float: "left",
        height: "70px"
    },
    wh30: {
        width: "30%",
        float: "left",
        height: "70px"
    },
    wh40: {
        width: "40%",
        float: "left",
        height: "70px"
    },
    w5: {
        width: "5%",
        float: "left",
    },
    w10: {
        width: "10%",
        float: "left",
    },
    w15: {
        width: "15%",
        float: "left",
    },
    w20: {
        width: "20%",
        float: "left",
    },
    w25: {
        width: "25%",
        float: "left",
    },
    w30: {
        width: "30%",
        float: "left",
    },
    w40: {
        width: "40%",
        float: "left",
    },
    row: {
        float: "left",
        width: "100%"
    },
    success: {
        color: 'green'
    },
    danger: {
        color: 'red'
    },
    mgl5: {
        marginLeft: '5px'
    },
    tags: {
        float: "right",
        marginRight: "5px",
        width: "250px"
    },
    searchInput: {
        width: "190px",
        display: 'inline-block'
    }
}

export default Ipfs;
