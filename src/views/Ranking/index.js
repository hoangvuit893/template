import React, { Component } from 'react';

import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    Table,
    Button, Input
} from 'reactstrap';
import 'moment-timezone';
import Pagination from "react-js-pagination";
let headers = new Headers();
const auth = localStorage.getItem('auth');
headers.append('Authorization', 'Bearer ' + auth);

class TwitterUsers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            tags: [],
            selectedTag: '',
            key: '',
            activePage: 1,
            page: 1,
            itemsCount: 0,
            limit: 20
        };
    }
    async componentDidMount() {
        const fetchData = {
            method: 'GET',
            headers: headers
        };
        fetch(global.BASE_URL + '/admin/tags', fetchData).then( cards => {
            cards.json().then(result => {
                this.setState({
                    tags: result.data,
                    selectedTag: result.data[0].hashTag
                }, () => {
                    if (result.data.length) {
                        this.getUsers()
                    }
                });
            });
        }).catch(console.log);
    }
    async getUsers(page=1) {
        const limit = this.state.limit;
        const tag = this.state.selectedTag;
        const key = this.state.key || '';
        const fetchData = {
            method: 'GET',
            headers: headers
        };
        fetch(global.BASE_URL + '/admin/twitter-users?hashTag='+tag + '&key='+ key +'&sort=votes&direction=desc&page='+page+'&limit='+limit, fetchData).then( cards => {
            cards.json().then(result => {
                this.setState({
                    users: result.data,
                    itemsCount: result.total,
                    activePage: page
                });
            })
        }).catch(console.log);
    }
    toggle(action) {
        this.setState({
            modal: !this.state.modal,
            action,
            isLoading: false,
        });
    }
    async handlePageChange(pageNumber) {
        this.getUsers(pageNumber);
    }
    changeTag(e) {
        this.setState({selectedTag: e.target.value}, () => {
            this.getUsers();
        });
    }
    inputChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    goSearch() {
        this.getUsers();
    }
    render() {
        const {users, tags, key} = this.state;
        let no = (this.state.activePage-1)*this.state.limit;
        if(!this.state.isLoading) {
            return (
                <div className="animated fadeIn">
                    <Row>
                        <Col>
                            <p style={styles.success}>{this.state.created}</p>
                            <p style={styles.danger}>{this.state.deleted}</p>
                            <Card>
                                <CardHeader>
                                    <i className="fa fa-align-justify"></i> RANKING
                                    <div style={styles.tags}>
                                        <div>
                                            <Input style={styles.searchInput} onChange={(e) => this.inputChange(e)} name="key" value={key} placeholder="Search"/>
                                            <Button outline color="success" style={styles.floatRight} size="sm"  onClick={e=>this.goSearch()}>Search</Button>
                                        </div>
                                    </div>
                                    <div style={styles.tags}>
                                        <label htmlFor="tag">Tags:</label>
                                        <select id="tag" style={styles.searchInput} className="form-control" onChange={(e) => this.changeTag(e)}>
                                            {
                                                tags.map((tag, i) => {
                                                    return (
                                                        <option key={i} value={tag.hashTag}>{tag.hashTag}</option>
                                                    )
                                                })
                                            }
                                        </select>
                                    </div>
                                </CardHeader>
                                <CardBody>
                                    <Table responsive>
                                        <thead>
                                        <tr>
                                            <th style={styles.wh5}>No.</th>
                                            <th style={styles.wh20}>ID</th>
                                            <th style={styles.wh15}>Screen Name</th>
                                            <th style={styles.wh15}>Name</th>
                                            <th style={styles.wh10}>Image</th>
                                            <th style={styles.wh10}>Votes</th>
                                            <th style={styles.wh10}>Profile</th>
                                            {/*<th style={styles.wh10}>Hidden</th>*/}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            users.map((item, i) => {
                                                return (
                                                    <tr key={i} style={styles.row} className={item.blocked ? "expired" : ""}>
                                                        <td style={styles.w5}>{ ++no }</td>
                                                        <td style={styles.w20}>{item.id}</td>
                                                        <td style={styles.w15}>{item.screen_name}</td>
                                                        <td style={styles.w15}>{item.name}</td>
                                                        <td style={styles.w10}><a href={item.profile_image_url} target="_blank">View</a></td>
                                                        <td style={styles.w10}>
                                                            {item.votes}
                                                        </td>
                                                        <td style={styles.wh10}><a href={"https://twitter.com/"+item.screen_name} target="_blank">View</a></td>
                                                        {/*<td style={styles.w10}>*/}
                                                            {/*<Input key={no} type="checkbox" value={item.id} onChange={()=>this.onChangeBlock(item.id, !item.blocked, i)} checked={item.blocked||false}/>*/}
                                                        {/*</td>*/}
                                                    </tr>
                                                );
                                            })
                                        }
                                        </tbody>
                                    </Table>
                                </CardBody>
                            </Card>
                            <Pagination
                                activePage={this.state.activePage}
                                itemsCountPerPage={this.state.limit}
                                totalItemsCount={this.state.itemsCount}
                                pageRangeDisplayed={10} // so luong item hien thi tren pagination number
                                onChange={e => this.handlePageChange(e)}
                                itemClass="page-item"
                                linkClass="page-link"
                            />
                        </Col>
                    </Row>
                </div>
            );
        }
        return (
            <div id="page-loading">
                <div className="three-balls">
                    <div className="ball ball1"></div>
                    <div className="ball ball2"></div>
                    <div className="ball ball3"></div>
                </div>
            </div>
        );
    }
}

const styles = {
    a : {
        textDecoration: 'none'
    },
    floatRight: {
        float: "right"
    },
    spinner: {
        width: "30px"
    },
    center: {
        textAlign: "center"
    },
    tbody: {
        height: "380px",
        overflowY: "auto"
    },
    wh5: {
        width: "5%",
        float: "left",
        height: "70px"
    },
    wh10: {
        width: "9.8%",
        float: "left",
        height: "70px"
    },
    wh15: {
        width: "15%",
        float: "left",
        height: "70px"
    },
    wh20: {
        width: "20%",
        float: "left",
        height: "70px"
    },
    wh25: {
        width: "25%",
        float: "left",
        height: "70px"
    },
    wh30: {
        width: "30%",
        float: "left",
        height: "70px"
    },
    wh40: {
        width: "40%",
        float: "left",
        height: "70px"
    },
    w5: {
        width: "5%",
        float: "left",
    },
    w10: {
        width: "10%",
        float: "left",
    },
    w15: {
        width: "15%",
        float: "left",
    },
    w20: {
        width: "20%",
        float: "left",
    },
    w25: {
        width: "25%",
        float: "left",
    },
    w30: {
        width: "30%",
        float: "left",
    },
    w40: {
        width: "40%",
        float: "left",
    },
    row: {
        float: "left",
        width: "100%"
    },
    success: {
        color: 'green'
    },
    danger: {
        color: 'red'
    },
    mgl5: {
        marginLeft: '5px'
    },
    tags: {
        float: "right",
        marginRight: "5px",
        width: "250px"
    },
    searchInput: {
        width: "190px",
        display: 'inline-block'
    }
}

export default TwitterUsers;
