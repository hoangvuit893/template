import React, { Component } from 'react';

import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    Table, Button, Input,
    ModalHeader, ModalBody, ModalFooter,Modal
} from 'reactstrap';
import 'moment-timezone';
import Pagination from "react-js-pagination";
let headers = new Headers();
const auth = localStorage.getItem('auth');
headers.append('Authorization', 'Bearer ' + auth);
headers.append('Content-Type', 'application/json');
class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            key: '',
            activePage: 1,
            page: 1,
            itemsCount: 0,
            limit: 20,
            totalActive: 0,
            modalCom: false,
            viewingUser: {},
            communities: [],
            updated: '',
        };
    }
    async componentDidMount() {
        this.getUsers();
    }
    getUsers(page = 1) {
        const limit = this.state.limit;
        const key = this.state.key || '';
        const fetchData = {
            method: 'GET',
            headers: headers
        };
        fetch(global.BASE_URL + '/admin/users?key='+ key +'&page='+page+'&limit='+limit, fetchData).then( users => {
            users.json().then(result => {
                this.setState({
                    data: result.data,
                    itemsCount: result.total,
                    activePage: page,
                    totalActive: result.totalActive,
                    updated: '',
                });
            })
        }).catch(console.log);
    }
    async handlePageChange(pageNumber) {
        this.getUsers(pageNumber);
    }
    toggle(action = '') {
        this.setState({
            modal: !this.state.modal,
            image: '',
            url: '',
            isActive: false,
            isLoading: false,
            errors: {},
            action,
            position: 1,
            data: [],
            updated: '',
        });
    }
    inputChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    goSearch() {
        this.getUsers();
    }
    toggleChannel() {
        this.setState({
            modalCom: !this.state.modalCom,
            viewingUser: {}
        });
    }
    execUpdateCommunity(user) {
        const fetchData = {
            method: 'GET',
            headers: headers
        };
        fetch(global.BASE_URL + '/admin/user/'+user.address+'/admin-communities', fetchData).then( coms => {
            coms.json().then(result => {
                this.setState({
                    modalCom: true,
                    viewingUser: user,
                    communities: result.data
                });
            })
        }).catch(console.log);
    }
    setUserCommunityAdmin (address, communityId, admin = true) {
        const fetchData = {
            method: 'PATCH',
            headers: headers,
            body: JSON.stringify({address, communityId, admin})
        };
        let updated = 'Set admin successfully';
        let message = 'Are you sure to set admin?';
        if (!admin) {
            message = 'Are you sure to remove admin?';
            updated = 'Remove admin successfully';
        }
        if (window.confirm(message)) {
            fetch(global.BASE_URL + '/admin/community/set-community-admin/', fetchData).then(() => {
                this.getUsers()
                this.setState({
                    modalCom: false,
                    viewingUser: {},
                    communities: [],
                    updated,
                });
            }).catch(console.log);
        }
    }
    render() {
        const {data, key, viewingUser, communities} = this.state;
        if(!this.state.isLoading) {
            return (
                <div className="animated fadeIn">
                    <Row>
                        <Col>
                            <p style={styles.success}>{this.state.updated}</p>
                            <p style={styles.danger}>{this.state.deleted}</p>
                            <Card>
                                <CardHeader>
                                    <i className="fa fa-align-justify"></i> USERS (Total: {this.state.itemsCount}, Active: {this.state.totalActive})
                                    <div style={styles.tags}>
                                        <div>
                                            <Input style={styles.searchInput} onChange={(e) => this.inputChange(e)} name="key" value={key} placeholder="Search"/>
                                            <Button outline color="success" style={styles.floatRight} size="sm"  onClick={e=>this.goSearch()}>Search</Button>
                                        </div>
                                    </div>
                                </CardHeader>
                                <CardBody>
                                    <Table responsive>
                                        <thead>
                                        <tr>
                                            <th style={styles.wh5}>No.</th>
                                            <th style={styles.wh15}>Username<br/>ユーザー名</th>
                                            <th style={styles.wh15}>Address<br/>アドレス</th>
                                            <th style={styles.wh15}>Email<br/>メールアドレス</th>
                                            <th style={styles.wh10}>Avatar<br/>アバター</th>
                                            <th style={styles.wh10}>Status<br/>ステータス</th>
                                            <th style={styles.wh20}></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            data.map((item, i) => {
                                                return (
                                                    <tr key={i} style={styles.row}>
                                                        <td style={styles.w5}>{ i+1}</td>
                                                        <td style={styles.w15}>{item.username}</td>
                                                        <td style={styles.w15}>{item.address}</td>
                                                        <td style={styles.w15}>{item.email}</td>
                                                        <td style={styles.w10}><a href={item.image} target="_blank">{item.image? 'View image' : ''}</a></td>
                                                        <td style={styles.w10}><span style={!item.active?styles.userPending:null}>{item.active ? 'Active' : 'Pending'}</span></td>
                                                        <td style={styles.w20}>
                                                            {
                                                                item.active ? <Button outline color="primary" size="sm"
                                                                                      onClick={(e) => this.execUpdateCommunity(item)}>Update
                                                                    Community Admin</Button> : ''
                                                            }
                                                            {/*<Button outline color="danger" size="sm" onClick={(e) => this.deleteCard(item._id) }>Delete</Button>*/}
                                                        </td>
                                                    </tr>
                                                );
                                            })
                                        }
                                        </tbody>
                                    </Table>
                                </CardBody>
                            </Card>
                            <Pagination
                                activePage={this.state.activePage}
                                itemsCountPerPage={this.state.limit}
                                totalItemsCount={this.state.itemsCount}
                                pageRangeDisplayed={10} // so luong item hien thi tren pagination number
                                onChange={e => this.handlePageChange(e)}
                                itemClass="page-item"
                                linkClass="page-link"
                            />
                        </Col>
                    </Row>

                    <Modal isOpen={this.state.modalCom} toggle={e=>this.toggleChannel()} className={this.props.className}>
                        <ModalHeader toggle={e=>this.toggleChannel()}>User: {viewingUser.username || viewingUser.address}</ModalHeader>
                        <ModalBody>
                            <Table>
                                <thead>
                                <th>No.</th>
                                <th style={styles.nagemonNameCol}>Community</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                {
                                        (communities.map((v, i) => {
                                                return (
                                                    <tr key={v._id}>
                                                        <td>{i+1}</td>
                                                        <td>{v.name}</td>
                                                        <td style={styles.center}>
                                                            {
                                                                v.admin ? <Button outline color="danger" size="sm" onClick={(e) => this.setUserCommunityAdmin(viewingUser.address, v._id, false) }>Remove Admin</Button>: <Button outline color="primary" size="sm" onClick={() => this.setUserCommunityAdmin(viewingUser.address, v._id, true) }>Set Admin</Button>
                                                            }
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                        )
                                }
                                </tbody>
                            </Table>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="secondary" onClick={e=>this.toggleChannel()}>Close</Button>
                        </ModalFooter>
                    </Modal>
                </div>
            );
        }
        return (
            <div id="page-loading">
                <div className="three-balls">
                    <div className="ball ball1"></div>
                    <div className="ball ball2"></div>
                    <div className="ball ball3"></div>
                </div>
            </div>
        );
    }
}

const styles = {
    a : {
        textDecoration: 'none'
    },
    floatRight: {
        float: "right"
    },
    spinner: {
        width: "30px"
    },
    center: {
        textAlign: "center"
    },
    tbody: {
        height: "380px",
        overflowY: "auto"
    },
    wh5: {
        width: "5%",
        float: "left",
        height: "70px"
    },
    wh10: {
        width: "9.8%",
        float: "left",
        height: "70px"
    },
    wh15: {
        width: "15%",
        float: "left",
        height: "70px"
    },
    wh20: {
        width: "20%",
        float: "left",
        height: "70px"
    },
    wh25: {
        width: "25%",
        float: "left",
        height: "70px"
    },
    wh30: {
        width: "30%",
        float: "left",
        height: "70px"
    },
    wh40: {
        width: "40%",
        float: "left",
        height: "70px"
    },
    w5: {
        width: "5%",
        float: "left",
    },
    w10: {
        width: "10%",
        float: "left",
    },
    w15: {
        width: "15%",
        float: "left",
    },
    w20: {
        width: "20%",
        float: "left",
    },
    w25: {
        width: "25%",
        float: "left",
    },
    w30: {
        width: "30%",
        float: "left",
    },
    w40: {
        width: "40%",
        float: "left",
    },
    row: {
        float: "left",
        width: "100%"
    },
    success: {
        color: 'green'
    },
    danger: {
        color: 'red'
    },
    mgl5: {
        marginLeft: '5px'
    },
    tags: {
        float: "right",
        marginRight: "5px",
        width: "250px"
    },
    searchInput: {
        width: "190px",
        display: 'inline-block'
    },
    userPending: {
        color: 'red'
    },
    nagemonNameCol: {
        width: '328px'
    }
}

export default Users;
