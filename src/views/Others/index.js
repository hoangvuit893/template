import React, { Component } from 'react';

import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  NavLink,
  Button,
  Input,
  Table,
} from 'reactstrap';
import validateInput from '../../shared/validations/commissionFee';
import _ from "lodash";
let headers = new Headers();
const auth = localStorage.getItem('auth');
headers.append('Authorization', 'Bearer ' + auth);
headers.append('Content-Type', 'application/json');

class Setting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      isLoading: false,
      updated: '',
      updatedAt: null,
      others: []
    };
  }
  async componentWillMount() {
    const fetchData = {
      method: 'GET',
      headers: headers
    };
      fetch(global.BASE_URL + '/setting', fetchData).then(async item => {
          const results = await item.json();
          let others = [];
          _.forIn(results.others, function (value, key) { others = [...others, { key: key, value: value }] });
          this.setState({
              others: others || [],
              updatedAt: new Date(results.updatedAt)
          });
      }).catch(console.log);
  }

  validate() {
    for (let i = 0; i < this.state.others.length; i++) {
      if (this.state.others[i].key !== '' && /^[A-Za-z_0-9]+$/.test(this.state.others[i].key)) {
      } else {
        return false;
      }
    }
    return true;
  }
  save() {
      const fetchData = {
          method: 'PUT',
          headers: headers,
          body: JSON.stringify({
              others: this.state.others.reduce((total, item) => ({ ...total, [item.key]: item.value }), {}),
              lastUpdated: this.state.updatedAt
          }),
      };
      fetch(global.BASE_URL + '/setting', fetchData).then(async (res) => {
          if (res.status === 200) {
              const result = await res.json();
              this.setState({ isLoading: false, updated: 'Data saved!', updatedAt: result.updatedAt});
          } else {
              alert('Something goes wrong. Please refresh page and try again.');
              this.setState({ isLoading: false });
          }
      }).catch(console.log);
  }
  isValid() {
    const { errors, isValid } = validateInput(this.state);
    if (!isValid) {
      this.setState({ errors });
    }
    return isValid;
  }
  handleOthersChange(e, key) {
    this.setState({ others: this.state.others.map((val, idx) => idx === key ? { ...val, [e.target.name]: e.target.value } : val) })
  }
  handleSubmit(e) {
    e.preventDefault();
    this.save();
  }
  render() {
      const userStr = localStorage.getItem('user');
      const user = userStr ? JSON.parse(userStr) : {};
    if (!this.state.isLoading) {
      return (
        <div className="animated fadeIn">
          <Row>
            <Col>
              <p style={styles.success}>{this.state.updated}</p>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> SETTINGS
                  <NavLink style={styles.floatRight} href="#/dashboard">Back</NavLink>
                </CardHeader>
                <CardBody>
                  <form onSubmit={(e) => this.handleSubmit(e)}>
                    <Table responsive>
                      <thead>
                        <tr>
                          <th>Key</th>
                          <th>Value</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          this.state.others.map((item, key) => {
                            if (!user.role || user.role === 'admin' || item.key === 'address_allowed_create_nft')
                              return (
                                  <tr key={key}>
                                      <td><Input required name="key" onChange={e => this.handleOthersChange(e, key)} placeholder="key" value={item.key} /></td>
                                      <td><Input name="value" onChange={e => this.handleOthersChange(e, key)} placeholder="value" value={item.value} /></td>
                                      <td><Button onClick={() => { this.setState({ others: this.state.others.filter((val, idx) => idx !== key) }) }} color="danger"><i className="fa fa-minus fa-sm"></i></Button></td>
                                  </tr>
                              )
                          })
                        }
                      </tbody>
                    </Table>
                    <div style={{ textAlign: "right" }}>
                      <Button onClick={() => this.setState({ others: [...this.state.others, { key: "", value: "", description: "" }] })} color="link">+Add row</Button>
                    </div>
                    <Button color="primary" type="submit" disabled={this.state.isLoading}>Save</Button>
                  </form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      );
    }
    return (
      <div id="page-loading">
        <div class="three-balls">
          <div class="ball ball1"></div>
          <div class="ball ball2"></div>
          <div class="ball ball3"></div>
        </div>
      </div>
    );
  }
}
const styles = {
  a: {
    textDecoration: 'none'
  },
  floatRight: {
    float: "right",
  },
  center: {
    textAlign: 'center'
  },
  padd: {
    padding: "5px"
  },
  spinner: {
    width: "30px"
  },
  fixed: {
    position: "fixed",
    top: "0px",
    display: "none",
    backgroundColor: "white"
  },
  floatLeftCenter: {
    float: "left",
    textAlign: "center"
  },
  floatLeft: {
    float: "left",
  },
  success: {
    color: 'green'
  },
  width250: {
    width: '250px'
  }
}


export default Setting;
