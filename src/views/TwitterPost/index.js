import React, { Component } from 'react';

import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    Table,
} from 'reactstrap';
import 'moment-timezone';
let headers = new Headers();
const auth = localStorage.getItem('auth');
headers.append('Authorization', 'Bearer ' + auth);

class TwitterUsers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            tags: [],
            summarizes: [],
            selectedTag: '',
            selectedDate: '',
            key: '',
            activePage: 1,
            page: 1,
            itemsCount: 0,
            limit: 20
        };
    }
    async componentDidMount() {
        const fetchData = {
            method: 'GET',
            headers: headers
        };
        fetch(global.BASE_URL + '/admin/tags', fetchData).then( cards => {
            cards.json().then(result => {
                this.setState({
                    tags: result.data,
                    selectedTag: result.data.length ? result.data[0].hashTag : ''
                }, () => {
                    if (result.data.length) {
                        this.getDates(result.data[0].hashTag)
                    }
                });
            });
        }).catch(console.log);
    }
    async getDates() {
        const tag = this.state.selectedTag;
        const fetchData = {
            method: 'GET',
            headers: headers
        };
        if (tag) {
            fetch(global.BASE_URL + '/summarizes?hashTag='+tag, fetchData).then( tags => {
                tags.json().then(res => {
                    this.setState({
                        summarizes: res.data,
                        selectedDate: res.data.length?res.data[0].rangeDate:''
                    }, () => {
                        if (res.data.length) {
                            this.getPosts()
                        }
                    });
                })
            }).catch(console.log);
        }
    }
    async getPosts() {
        const selectedDate = this.state.selectedDate;
        const tag = this.state.selectedTag;
        const fetchData = {
            method: 'GET',
            headers: headers
        };
        if (selectedDate) {
            fetch(global.BASE_URL + '/admin/summarizes/votes?rangeDate='+selectedDate+'&hashTag='+tag, fetchData).then(cards => {
                cards.json().then(result => {
                    this.setState({
                        users: result.data,
                        itemsCount: result.total,
                    });
                })
            }).catch(console.log);
        }
    }
    toggle(action) {
        this.setState({
            modal: !this.state.modal,
            action,
            isLoading: false,
        });
    }

    changeTag(e) {
        this.setState({selectedTag: e.target.value}, () => {
            this.getDates();
        });
    }
    changeSummarize(e) {
        this.setState({selectedDate: e.target.value}, () => {
            this.getPosts();
        });
    }

    render() {
        const {users, tags, key, summarizes} = this.state;
        let no = (this.state.activePage-1)*this.state.limit;
        if(!this.state.isLoading) {
            return (
                <div className="animated fadeIn">
                    <Row>
                        <Col>
                            <p style={styles.success}>{this.state.created}</p>
                            <p style={styles.danger}>{this.state.deleted}</p>
                            <Card>
                                <CardHeader>
                                    <i className="fa fa-align-justify"></i> TWITTER POSTS
                                    <div style={styles.tags}>
                                        <label htmlFor="tag">Tags:</label>
                                        <select id="tag" style={styles.searchInput} className="form-control" onChange={(e) => this.changeTag(e)}>
                                            {
                                                tags.map((tag, i) => {
                                                    return (
                                                        <option key={i} value={tag.hashTag}>{tag.hashTag}</option>
                                                    )
                                                })
                                            }
                                        </select>

                                        <select id="tag" style={styles.searchInput} className="form-control" onChange={(e) => this.changeSummarize(e)}>
                                            {
                                                summarizes.map((tag, i) => {
                                                    return (
                                                        <option key={i} value={tag.rangeDate}>{tag.rangeDate}</option>
                                                    )
                                                })
                                            }
                                        </select>
                                    </div>

                                </CardHeader>
                                <CardBody>
                                    <Table responsive>
                                        <thead>
                                        <tr>
                                            <th style={styles.wh5}>No.</th>
                                            <th style={styles.wh20}>Post URL<br/>投稿URL</th>
                                            <th style={styles.wh15}>Screen Name<br/>スクリーン名</th>
                                            <th style={styles.wh15}>Name<br/>名</th>
                                            <th style={styles.wh10}>Image<br/>イメージ</th>
                                            <th style={styles.wh10}>Votes<br/>投票</th>
                                            <th style={styles.wh10}>Profile<br/>プロファイル</th>
                                            <th style={styles.wh10}>Password<br/>パスワード</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            users.map((item, i) => {
                                                return (
                                                    <tr key={i} style={styles.row} className={item.passwordUsed ? "expired" : ""}>
                                                        <td style={styles.w5}>{ ++no }</td>
                                                        <td style={styles.w20}><a href={item.tweetId.link} target="_blank">{item.tweetId.link}</a></td>
                                                        <td style={styles.w15}>{item.user.screen_name}</td>
                                                        <td style={styles.w15}>{item.user.name}</td>
                                                        <td style={styles.w10}><a href={item.user.profile_image_url} target="_blank">View</a></td>
                                                        <td style={styles.w10}>
                                                            {item.value || 0}
                                                        </td>
                                                        <td style={styles.wh10}><a href={"https://twitter.com/"+item.user.screen_name} target="_blank">View</a></td>
                                                        <td style={styles.w10}>
                                                            {item.password}
                                                        </td>
                                                    </tr>
                                                );
                                            })
                                        }
                                        </tbody>
                                    </Table>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>
            );
        }
        return (
            <div id="page-loading">
                <div className="three-balls">
                    <div className="ball ball1"></div>
                    <div className="ball ball2"></div>
                    <div className="ball ball3"></div>
                </div>
            </div>
        );
    }
}

const styles = {
    a : {
        textDecoration: 'none'
    },
    floatRight: {
        float: "right"
    },
    spinner: {
        width: "30px"
    },
    center: {
        textAlign: "center"
    },
    tbody: {
        height: "380px",
        overflowY: "auto"
    },
    wh5: {
        width: "5%",
        float: "left",
        height: "70px"
    },
    wh10: {
        width: "9.8%",
        float: "left",
        height: "70px"
    },
    wh15: {
        width: "15%",
        float: "left",
        height: "70px"
    },
    wh20: {
        width: "20%",
        float: "left",
        height: "70px"
    },
    wh25: {
        width: "25%",
        float: "left",
        height: "70px"
    },
    wh30: {
        width: "30%",
        float: "left",
        height: "70px"
    },
    wh40: {
        width: "40%",
        float: "left",
        height: "70px"
    },
    w5: {
        width: "5%",
        float: "left",
    },
    w10: {
        width: "10%",
        float: "left",
    },
    w15: {
        width: "15%",
        float: "left",
    },
    w20: {
        width: "20%",
        float: "left",
    },
    w25: {
        width: "25%",
        float: "left",
    },
    w30: {
        width: "30%",
        float: "left",
    },
    w40: {
        width: "40%",
        float: "left",
    },
    row: {
        float: "left",
        width: "100%"
    },
    success: {
        color: 'green'
    },
    danger: {
        color: 'red'
    },
    mgl5: {
        marginLeft: '5px'
    },
    tags: {
        float: "right",
        marginRight: "5px",
        width: "450px"
    },
    searchInput: {
        width: "190px",
        display: 'inline-block'
    }
}

export default TwitterUsers;
