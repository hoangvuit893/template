import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  Button,
  Modal, ModalHeader, ModalBody, ModalFooter,
  Form,
  Dropdown, DropdownToggle, DropdownMenu, DropdownItem
} from 'reactstrap';
import 'moment-timezone';
import Pagination from "react-js-pagination";
import Moment from 'react-moment';

let headers = new Headers();
const auth = localStorage.getItem('auth');
headers.append('Authorization', 'Bearer ' + auth);
headers.append('Content-Type', 'application/json');

class WithdrawalInformation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      deleted: false,
      isLoading: false,
      errors: {},
      action: '',
      created: '',
      updateId: '',
      activePage: 1,
      numPage: 1,
      itemsCount: 0,
      itemPerPage: 20,
      firstName: '',
      lastName: '',
      country: '',
      zipcode: '',
      homeAddress: '',
      phoneNumber: '',
      email: '',
      sex: '',
      age: '',
      status: '',
      //hash: '',
      //address: '',
      dropdownOpen: false
    };
  }

  async componentDidMount() {
      this.loadData()
  }

  loadData = () => {
    const page = this.state.activePage || 1;
    const limit = this.state.itemPerPage || 200;
    const fetchData = {
      method: 'GET',
      headers: headers
    };

    fetch(global.BASE_URL + '/withdrawal-infos?limit=' + limit + '&page=' + page, fetchData).then(res => {
      res.json().then(result => {
        this.setState({
          data: result.data,
          itemsCount: result.totalItems,
          isLoading: false
        });
        console.log(result);
      })
    }).catch(console.log);
  }

  toggle(action) {
    this.setState({
      modal: !this.state.modal,
      action,
      isLoading: false,
      updateId:'',
      deleted: false,
      errors: {},
      firstName: '',
      lastName: '',
      country: '',
      zipcode: '',
      homeAddress: '',
      phoneNumber: '',
      email: '',
      sex: '',
      age: '',
      status: '',
      hash: '',
      address: '',
    });
  }
  execUpdate = (item) => {
    this.setState({
      action: 'update',
      firstName: item.firstName  || '',
      lastName: item.lastName  || '',
      country: item.country || '',
      zipcode: item.zipcode || '',
      homeAddress: item.homeAddress || '',
      phoneNumber: item.phoneNumber || '',
      email: item.email || '',
      sex: item.sex || '',
      age: item.age || '',
      status: item.status || '',
      hash: item.hash || '',
      address: item.address || '',
      updateId: item._id,
      modal: !this.state.modal,
    });
  } 

  cancelUpdate = () => {
    this.setState({
      modal: false,
    });
  }

  updateApp = () => {
    const id = this.state.updateId;
    // process utc time
    // end process utc time
    const body = {status: this.state.status};
    
    const fetchData = {
      method: 'PUT',
      headers: headers,
      body: JSON.stringify(body)
    };
    fetch(global.BASE_URL + '/withdrawal-infos/'+id, fetchData).then(async () => {
      this.cancelUpdate();
      this.loadData();
      this.setState({created: ' Status updated successfully', isLoading: false, deleted: false});
    }).catch(console.log);
  }

  handlePageChange = async pageNumber => {
      this.setState({activePage: pageNumber, isLoading: true}, () => {
          this.loadData();
      });
  };

  setDropdownOpen = () => {
    this.setState({dropdownOpen: !this.state.dropdownOpen})
  }

  setLastClicked(value) {
    this.setState({status: value})
  }

  render() {
    const {data, lastName, firstName, country, zipcode, homeAddress, phoneNumber, email, sex, age, status, hash, address} = this.state;
    const page = this.state.activePage;
    const limit = this.state.itemPerPage;
    if(!this.state.isLoading) {
      return (
        <div className="animated fadeIn">
          <Row>
            <Col>
              <p style={styles.success}>{this.state.created}</p>
              <p style={styles.danger}>{this.state.deleted}</p>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> WITHDRAWAL INFORMATION
                </CardHeader>
                <CardBody>
                  <Table responsive>
                    <thead>
                    <tr>
                      <th style={styles.wh5}>No</th>
                      <th style={styles.wh15}>First Name</th>
                      <th style={styles.wh15}>Last Name</th>
                      <th style={styles.wh10}>Phone</th>
                      <th style={styles.wh10}>Home Address</th>
                      <th style={styles.wh10}>Date</th>
                      <th style={styles.wh10}>Status</th>
                      <th style={styles.wh15}></th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                      data.map((item, i) => {
                        item.description_en = item.description_en || '';
                        item.description_jp = item.description_jp || '';
                        return (
                          <tr key={i} style={styles.row}>
                            <td style={styles.w5}>{(page-1)*limit+i+1}</td>
                            <td style={styles.w15}>{item.firstName}</td>
                            <td style={styles.w15}>{item.lastName}</td>
                            <td style={styles.w10}>{item.phoneNumber}</td>
                            <td style={styles.w10}>{item.homeAddress}</td>
                            <td style={styles.w10}><Moment format="YYYY/MM/DD">{item.createdAt}</Moment></td>
                            <th style={styles.w10}>{item.status}</th>
                            <td style={styles.w15}>
                              <Button outline color="primary" size="sm" onClick={() => this.execUpdate(item) }>Update Status</Button>
                              {/* &nbsp;
                              <Button outline color="danger" size="sm" onClick={() => this.deleteCard(item._id) }>Delete</Button> */}
                            </td>
                          </tr>
                        );
                      })
                    }
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
                <Pagination
                    activePage={this.state.activePage}
                    itemsCountPerPage={this.state.itemPerPage}
                    totalItemsCount={this.state.itemsCount}
                    pageRangeDisplayed={10}
                    onChange={e => this.handlePageChange(e)}
                    itemClass="page-item"
                    linkClass="page-link"
                />
            </Col>
          </Row>
          <Modal isOpen={this.state.modal} toggle={this.cancelUpdate} className={this.props.className}>
              <ModalHeader toggle={()=>this.toggle()}>Edit status</ModalHeader>
              <ModalBody>
                <Form>
                  <TextField title='First Name' value={firstName}/>
                  <TextField title='Last Name' value={lastName}/>
                  <TextField title='Country' value={country}/>
                  <TextField title='Zip code' value={zipcode}/>
                  <TextField title='Home Address' value={homeAddress}/>
                  <TextField title='Phone number' value={phoneNumber}/>
                  <TextField title='Mail address' value={email}/>
                  <TextField title='Sex' value={sex === 1 ? 'Female' : 'Male'}/>
                  <TextField title='Age' value={age}/>
                  <TextField title='hash' value={hash}/>
                  <TextField title='address' value={address}/>
                  <div className="position-relative form-group">
                    <div className="form-group">
                      <label className="control-label"><span style={styles.bold}>Status</span></label>
                      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.setDropdownOpen}>
                        <DropdownToggle caret>
                          {status}
                        </DropdownToggle>
                        <DropdownMenu container="body">
                          <DropdownItem onClick={() => this.setLastClicked('pending')}>pending</DropdownItem>
                          <DropdownItem onClick={() => this.setLastClicked('processing')}>processing</DropdownItem>
                          <DropdownItem onClick={() => this.setLastClicked('completed')}>completed</DropdownItem>
                        </DropdownMenu>
                      </Dropdown>
                    </div>
                  </div>
                </Form>
              </ModalBody>
              <ModalFooter>
                <Button color="primary" onClick={this.updateApp} disabled={this.state.isLoading}>Save Status</Button>&nbsp;&nbsp;
                <Button color="secondary" onClick={this.cancelUpdate}>Cancel</Button>
              </ModalFooter>
            </Modal>
        </div>
      );
    }
    return (
      <div id="page-loading">
        <div className="three-balls">
          <div className="ball ball1"></div>
          <div className="ball ball2"></div>
          <div className="ball ball3"></div>
        </div>
      </div>
    );
  }
}

function TextField(props) {
  return (
    <div className="position-relative form-group">
      <div className="form-group">
        <label className="control-label" style={styles.wrapWord}><span style={styles.bold}>{props.title}:</span> {props.value}</label>
      </div>
    </div>
  );
};

const styles = {
    a : {
        textDecoration: 'none'
    },
    floatRight: {
      float: "right"
    },
    spinner: {
      width: "30px"
    },
    center: {
      textAlign: "center"
    },
    tbody: {
      height: "380px",
      overflowY: "auto"
    },
    wh5: {
      width: "5%",
      float: "left",
      height: "70px"
    },
    wh10: {
      width: "9.8%",
      float: "left",
      height: "70px"
    },
    wh15: {
      width: "15%",
      float: "left",
      height: "70px"
    },
    wh20: {
        width: "20%",
        float: "left",
        height: "70px"
    },
    wh25: {
      width: "25%",
      float: "left",
      height: "70px"
    },
    wh30: {
      width: "30%",
      float: "left",
      height: "70px"
    },
    wh40: {
      width: "40%",
      float: "left",
      height: "70px"
    },
    w5: {
      width: "5%",
      float: "left",
    },
    w10: {
      width: "10%",
      float: "left",
    },
    w15: {
      width: "15%",
      float: "left",
    },
    w20: {
        width: "20%",
        float: "left",
    },
    w25: {
      width: "25%",
      float: "left",
    },
    w30: {
      width: "30%",
      float: "left",
    },
    w40: {
      width: "40%",
      float: "left",
    },
    row: {
      float: "left",
      width: "100%"
    },
    success: {
      color: 'green'
    },
    danger: {
      color: 'red'
    },
    mgl5: {
      marginLeft: '5px'
    },
    bold: {
      fontWeight: 'bold' 
    },
    wrapWord: {
      wordWrap: 'anywhere'
    }
}

export default WithdrawalInformation;
