import React, { Component } from 'react';

import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  NavLink,
  Button
} from 'reactstrap';
import TextArea from '../Common/TextArea';
let headers = new Headers();
const auth = localStorage.getItem('auth');
headers.append('Authorization', 'Bearer ' + auth);

class Setting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: '',
      errors: {},
      isLoading: false,
    };
  }
  async componentWillMount() {
    const fetchData = {
      method: 'GET',
      headers: headers
    };
    fetch(global.BASE_URL + '/notification_setting/', fetchData).then(async item => {
        const results = await item.json();
        this.setState({
          data: JSON.stringify(results,  null, '\t')
        });
    }).catch(console.log);
  }
  onChange(e) {
    this.setState({
      data: e.target.value
    });
  }
  save(){
    this.setState({ isLoading: true });
    if (this.isJson(this.state.data)) {
      const fetchData = {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify({
          data: this.state.data
        }),
      };
      fetch(global.BASE_URL + '/notification_setting', fetchData).then((res) => {
        if (res.status === 200) {
          this.setState({isLoading: false});
        } else {
          alert('Something goes wrong. Please try again.');
          this.setState({isLoading: false});
        }
      }).catch(console.log);
    }
    else {
      alert('JSON is valid');
      this.setState({isLoading: false});
    }
  }
  isJson(text) {
    if (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
      replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
      replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
        return true;
      }
    return false;
  }
  render() {
    const {errors} = this.state;
    if(!this.state.isLoading) {
      return (
        <div className="animated fadeIn">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> NOTIFICATION SETTINGS
                  <NavLink style={styles.floatRight} href="#/dashboard">Back</NavLink>
                </CardHeader>
                <CardBody>
                <form>
                  <TextArea
                            field="data"
                            label="Setting"
                            value={this.state.data}
                            rows="15"
                            onChange={e=>this.onChange(e)}
                            />
                  <Button color="primary" onClick={e=>this.save()} disabled={this.state.isLoading}>Save</Button>
                </form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      );
    }
    return (
      <div id="page-loading">
        <div class="three-balls">
          <div class="ball ball1"></div>
          <div class="ball ball2"></div>
          <div class="ball ball3"></div>
        </div>
      </div>
    );
  }
}
const styles = {
  a : {
      textDecoration: 'none'
  },
  floatRight: {
    float: "right",
  },
  center: {
    textAlign: 'center'
  },
  padd: {
    padding: "5px"
  },
  spinner: {
    width: "30px"
  },
  fixed: {
    position: "fixed",
    top: "0px",
    display: "none",
    backgroundColor: "white"
  },
  floatLeftCenter:{
    float: "left",
    textAlign: "center"
  },
  floatLeft: {
    float: "left",
  }
}


export default Setting;
