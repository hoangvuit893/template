import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter,
    Form, FormGroup } from 'reactstrap';
import TextFieldGroup from "../Common/TextFieldGroup";
import DateTimePicker from 'react-datetime-picker';
import validateInput from '../../shared/validations/HashTag';
import TextArea from "../Common/TextArea";
let headers = new Headers();
const auth = localStorage.getItem('auth');
headers.append('Authorization', 'Bearer ' + auth);

class Logs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            errors: {},
            activePage: 1,
            numPage: 1,
            itemsCount: 0,
            itemPerPage: 10,
            isLoading: false,
            hashTag: '',
            startAt: '',
            endAt: '',
            endVoteAt: '',
            forUsers: '',
            updateId: '',
        };
    }
    async componentDidMount() {
        const fetchData = {
            method: 'GET',
            headers: headers,
            json: true
        };
        fetch(global.BASE_URL + '/admin/tags', fetchData).then( async result => {
            result = await result.json();
            this.setState({
                data: result.data,
            });
        }).catch(console.log);
    }
    toggle() {
        this.setState({
            modal: !this.state.modal,
            updateId: '',
            hashTag: '',
            startAt: '',
            endAt: '',
            endVoteAt: '',
            action: '',
            forUsers: ''
        });
    }
    onStartAtChange = (date) => {
        console.log(date)
        this.setState({ startAt: date });
    }
    onEndAtChange = date => {
        this.setState({ endAt: date });
    }
    onEndVoteAtChange = date => {
        this.setState({ endVoteAt: date });
    }
    inputChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    updateApp() {
        if (this.isValid()) {
            this.setState({ errors: {}, isLoading: true });
            const id = this.state.updateId;
            // process utc time
            // end process utc time
            const tmpForUsers = this.state.forUsers.trim().split('\n');
            const forUsers = [];
            for (let i = 0; i < tmpForUsers.length; i++) {
                if (tmpForUsers[i].trim() !== '') {
                    forUsers.push(tmpForUsers[i]);
                }
            }
            let body = {
                startAt: this.state.startAt,
                endAt: this.state.endAt,
                endVoteAt: this.state.endVoteAt,
                forUsers
            };
            if (id) {
                const fetchData = {
                    method: 'PUT',
                    headers: headers,
                    body: JSON.stringify(body)
                };
                fetch(global.BASE_URL + '/tag/'+id, fetchData).then(async () => {
                    this.toggle();
                    this.componentDidMount();
                    this.setState({created: 'Tag updated successfully', isLoading: false, deleted: ''});
                }).catch(() => {
                    this.toggle();
                });
            } else {
                body['hashTag'] = this.state.hashTag
                const fetchData = {
                  method: 'POST',
                  headers: headers,
                  body: JSON.stringify(body)
                };
                fetch(global.BASE_URL + '/tag/', fetchData).then(async () => {
                  this.toggle();
                  this.componentDidMount();
                  this.setState({created: 'Tag created successfully', isLoading: false, deleted: ''});
                }).catch(() => {
                    this.toggle();
                });
            }
        }
    }
    execDelete(item) {
        if (window.confirm('Are you sure to delete ' + item.hashTag + '?')) {
            const fetchData = {
                method: 'DELETE',
                headers: headers,
            };
            fetch(global.BASE_URL + '/tag/' + item._id, fetchData).then(async () => {
                this.componentDidMount();
                this.setState({created: 'Item removed', isLoading: false, deleted: ''});
            }).catch(() => {
            });
        }
    }
    isValid() {
        const { errors, isValid } = validateInput(this.state);
        this.setState({ errors });
        return isValid;
    }
    execUpdate = (item) => {
        const forUsers = item.forUsers || [];
        this.setState({
            action: 'update',
            updateId: item._id,
            hashTag: item.hashTag,
            startAt: new Date(item.startAt),
            endAt: new Date(item.endAt),
            endVoteAt: new Date(item.endVoteAt),
            forUsers: forUsers.join('\n'),
            modal: !this.state.modal,
        });
    }
    render() {
        const { hashTag, errors, startAt, endAt, endVoteAt, forUsers } = this.state;
        if (this.state.isLoading) {
            return (
                <div id="page-loading">
                    <div className="three-balls">
                        <div className="ball ball1"></div>
                        <div className="ball ball2"></div>
                        <div className="ball ball3"></div>
                    </div>
                </div>
            );
        }
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col xl={12}>
                        <p style={styles.success}>{this.state.updated}</p>
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify"></i> Hash Tags
                                <Button outline color="primary" style={styles.floatRight} size="sm"  onClick={e=>this.toggle()}>Add</Button>
                            </CardHeader>
                            <CardBody>
                                <Table responsive hover>
                                    <thead>
                                    <tr>
                                        <th style={styles.wh5} scope="col">no.</th>
                                        <th style={styles.wh20} scope="col">Hash Tag<br/>ハッシュタグ</th>
                                        <th style={styles.wh15} scope="col">Start Time<br/>スタートタイム</th>
                                        <th style={styles.wh15} scope="col">End Time<br/>終了タイム</th>
                                        <th style={styles.wh15} scope="col">Voting End Time<br/>投票終了タイム</th>
                                        {/*<th style={styles.wh10} scope="col">Note<br/></th>*/}
                                        <th style={styles.wh15} scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.state.data.map((item, index) => {
                                            const note = item.endVoteAt&&new Date(item.endVoteAt).getTime() < new Date().getTime() ? 'Expired': ''
                                            return (
                                                <tr key={item._id.toString()} className={note? 'expired' : ''}>
                                                    <th scope="row" style={styles.w5}>{index + 1}</th>
                                                    <td style={styles.w20}>{item.hashTag||''}</td>
                                                    <td style={styles.w15}>{new Date(item.startAt).toLocaleString()}</td>
                                                    <td style={styles.w15}>{new Date(item.endAt).toLocaleString()}</td>
                                                    <td style={styles.w15}>{new Date(item.endVoteAt).toLocaleString()}</td>
                                                    {/*<td style={styles.w10}>{*/}
                                                        {/*note*/}
                                                    {/*}</td>*/}
                                                    <td style={styles.w15}>
                                                        <Button outline color="primary" size="sm" onClick={() => this.execUpdate(item) }>Update</Button>&nbsp;
                                                        <Button outline color="danger" size="sm" onClick={() => this.execDelete(item) }>Delete</Button>&nbsp;
                                                    </td>
                                                </tr>
                                            )
                                        }
                                    )}
                                    </tbody>
                                </Table>

                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Modal isOpen={this.state.modal} toggle={()=>this.toggle()} className={this.props.className}>
                    <ModalHeader toggle={()=>this.toggle()}>{this.state.action === 'update'? 'Edit' : 'Create Hash Tag'} {this.state.title ? Object.values(this.state.title)[0] : ''}</ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup>
                                <TextFieldGroup
                                    field="hashTag"
                                    label="Hash Tag ハッシュタグ"
                                    value={hashTag}
                                    error={errors.hashTag}
                                    onChange={e=>this.state.action !== 'update' ?this.inputChange(e) : {}}
                                />
                            </FormGroup>
                        </Form>
                        <div className="position-relative form-group">
                            <div className="form-group">
                                <label className="control-label">Start Time (Local time) スタートタイム</label>
                                <DateTimePicker
                                    onChange={this.onStartAtChange}
                                    value={startAt ? new Date(startAt) : ''}
                                    format={"y-MM-dd h:mm:ss a"}
                                    disableClock={true}
                                />
                                <span className="help-block error">{this.state.errors.startAt}</span>
                            </div>
                        </div>
                        <div className="position-relative form-group">
                            <div className="form-group">
                                <label className="control-label">End Time (Local time) 終了タイム</label>
                                <DateTimePicker
                                    onChange={this.onEndAtChange}
                                    value={endAt ? new Date(endAt): ''}
                                    format={"y-MM-dd h:mm:ss a"}
                                    disableClock={true}
                                />
                                <span className="help-block error">{this.state.errors.endAt}</span>
                            </div>
                        </div>
                        <div className="position-relative form-group">
                            <div className="form-group">
                                <label className="control-label">Voting End Time (Local time) 投票終了タイム</label>
                                <DateTimePicker
                                    onChange={this.onEndVoteAtChange}
                                    value={endVoteAt ? new Date(endVoteAt): ''}
                                    format={"y-MM-dd h:mm:ss a"}
                                    disableClock={true}
                                />
                                <span className="help-block error">{this.state.errors.endVoteAt}</span>
                            </div>
                        </div>
                        <Form>
                            <FormGroup>
                                <TextArea
                                    field="forUsers"
                                    label="Enable Users (each user per row) 取得するユーザースクリーン名（各人は各行に）"
                                    value={forUsers}
                                    error={errors.forUsers}
                                    rows={5}
                                    onChange={e=>this.inputChange(e)}
                                />
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={e=>this.updateApp()} disabled={this.state.isLoading}>Save</Button>{' '}
                        <Button color="secondary" onClick={e=>this.toggle()}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}
const styles = {
    a : {
        textDecoration: 'none'
    },
    floatRight: {
        float: "right"
    },
    spinner: {
        width: "30px"
    },
    center: {
        textAlign: "center"
    },
    tbody: {
        height: "380px",
        overflowY: "auto"
    },
    wh5: {
        width: "5%",
        float: "left",
        height: "70px"
    },
    wh7: {
        width: "7%",
        float: "left",
        height: "70px"
    },
    wh10: {
        width: "9.8%",
        float: "left",
        height: "70px"
    },
    wh15: {
        width: "15%",
        float: "left",
        height: "70px"
    },
    wh18: {
        width: "18%",
        float: "left",
        height: "70px"
    },
    wh20: {
        width: "20%",
        float: "left",
        height: "70px"
    },
    wh25: {
        width: "25%",
        float: "left",
        height: "70px"
    },
    wh30: {
        width: "30%",
        float: "left",
        height: "70px"
    },
    wh40: {
        width: "40%",
        float: "left",
        height: "70px"
    },
    w5: {
        width: "5%",
        float: "left",
    },
    w7: {
        width: "7%",
        float: "left",
    },
    w10: {
        width: "10%",
        float: "left",
    },
    w15: {
        width: "15%",
        float: "left",
    },
    w18: {
        width: "18%",
        float: "left",
    },
    w20: {
        width: "20%",
        float: "left",
    },
    w25: {
        width: "25%",
        float: "left",
    },
    w30: {
        width: "30%",
        float: "left",
    },
    w40: {
        width: "40%",
        float: "left",
    },
    row: {
        float: "left",
        width: "100%"
    },
    success: {
        color: 'green'
    },
    danger: {
        color: 'red'
    },
    mgl5: {
        marginLeft: '5px'
    },
    totalbox: {
        float: "right",
        marginTop: '4px'
    },
    expired: {
        background: "red"
    }
}
export default Logs;
