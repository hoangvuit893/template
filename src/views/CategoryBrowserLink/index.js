import React, { Component } from 'react';

import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  Button,
  Modal, ModalHeader, ModalBody, ModalFooter,
  Form, Input
} from 'reactstrap';
import 'moment-timezone';
let headers = new Headers();
const auth = localStorage.getItem('auth');
headers.append('Authorization', 'Bearer ' + auth);

class Cards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
        title: '',
        position: '',
        // subtitle: '',
        // isHide: false,
        isActive100: false,
        isLoading: false,
        errors: {},
        action: '',
        // position: '',
    };
  }
  async componentDidMount() {
    const fetchData = {
      method: 'GET',
      headers: headers
    };
    fetch(global.BASE_URL + '/admin/browser-link-categories', fetchData).then( cards => {
      cards.json().then(result => {
        this.setState({
          data: result.data
        });
      })
    }).catch(console.log);
  }
  toggle(action) {
    this.setState({
      modal: !this.state.modal,
      action,
      isLoading: false,
      updateId:'',
      title: '',
      position: '',
      isHide: false,
      isActive100: false,
      errors: {}
    });
  }
  // inputChange(e) {
  //   this.setState({ [e.target.name]: e.target.value });
  // }
  execUpdate = (item) => {
      this.setState({
          action: 'update',
          updateId: item._id,
          isActive100: item.isActive100,
          modal: !this.state.modal,
          // position: item.position
      });
  }
  cancelCreate() {
    this.setState({
      modal: false,
    });
  }
  updateApp= () => {
    if(this.isValid()) {
      this.setState({ errors: {}, isLoading: true });
      const id = this.state.updateId;
      // process utc time
      // end process utc time
      const body = {
                    isActive100: this.state.isActive100,
                  };
      if (this.state.action === 'update') {
        const fetchData = {
          method: 'PUT',
          headers: headers,
          body: JSON.stringify(body)
        };
        fetch(global.BASE_URL + '/browser-link-category/'+id, fetchData).then(async () => {
          this.cancelCreate();
          this.componentDidMount();
          this.setState({created: 'Link updated successfully', isLoading: false, deleted: ''});
        }).catch(console.log);
      } else {
        // const fetchData = {
        //   method: 'POST',
        //   headers: headers,
        //   body: JSON.stringify(body)
        // };
        // fetch(global.BASE_URL + '/browser-link/', fetchData).then(async () => {
        //   this.cancelCreate();
        //   this.componentDidMount();
        //   this.setState({created: 'Link created successfully', isLoading: false, deleted: ''});
        // }).catch(console.log);
      }
    }
  }
  isValid() {
    // const {errors, isValid } = validateInput(this.state);
    // if(!isValid) {
    //   this.setState({errors});
    // }
    // return isValid;
      return true;
  }
  // onChangeHide(e) {
  //   const checked = e.target.checked;
  //   this.setState({isHide: checked});
  // }

    onChangeAct(e) {
        const checked = e.target.checked;
        this.setState({[e.target.name]: checked});
    }

  render() {
    const {isActive100} = this.state;
    if(!this.state.isLoading) {
      return (
        <div className="animated fadeIn">
          <Row>
            <Col>
              <p style={styles.success}>{this.state.created}</p>
              <p style={styles.danger}>{this.state.deleted}</p>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> BROWSER LINKS CATEGORIES
                </CardHeader>
                <CardBody>
                  <Table responsive>
                    <thead>
                    <tr>
                      <th style={styles.wh5}>No.</th>
                      <th style={styles.wh15}>Title</th>
                      <th style={styles.wh10}>Position</th>
                      <th style={styles.wh15}></th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                      this.state.data.map((item, i) => {
                          return (
                            <tr key={i} style={styles.row}>
                              <td style={styles.w5}>{ i+1}</td>
                              <td style={styles.w15}>{item.title ? Object.values(item.title)[0] : ''}</td>
                                <td style={styles.w10}>{item.position}</td>
                              <td style={styles.w15}>
                                  <Button outline color="primary" size="sm" onClick={(e) => this.execUpdate(item) }>Update</Button>&nbsp;
                              </td>
                            </tr>
                          );
                      })
                    }
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Modal isOpen={this.state.modal} toggle={e=>this.cancelCreate()} className={this.props.className}>
              <ModalHeader toggle={()=>this.toggle()}>{this.state.action === 'update'? 'Edit' : 'Create '} {this.state.title ? Object.values(this.state.title)[0] : ''}</ModalHeader>
              <ModalBody>
                <Form>
                <div className="position-relative form-group">
                    <div className="form-group">
                        <label className="control-label">Active Version 1.0.0 <Input type="checkbox" name="isActive100" value="1"  onClick={e => this.onChangeAct(e)} defaultChecked={isActive100}/>
                        </label>
                    </div>
                </div>
                </Form>
              </ModalBody>
              <ModalFooter>
                <Button color="primary" onClick={e=>this.updateApp()} disabled={this.state.isLoading}>Save</Button>{' '}
                <Button color="secondary" onClick={e=>this.cancelCreate()}>Cancel</Button>
              </ModalFooter>
            </Modal>
        </div>
      );
    }
    return (
      <div id="page-loading">
        <div className="three-balls">
          <div className="ball ball1"></div>
          <div className="ball ball2"></div>
          <div className="ball ball3"></div>
        </div>
      </div>
    );
  }
}

const styles = {
    a : {
        textDecoration: 'none'
    },
    floatRight: {
      float: "right"
    },
    spinner: {
      width: "30px"
    },
    center: {
      textAlign: "center"
    },
    tbody: {
      height: "380px",
      overflowY: "auto"
    },
    wh5: {
      width: "5%",
      float: "left",
      height: "70px"
    },
    wh10: {
      width: "9.8%",
      float: "left",
      height: "70px"
    },
    wh15: {
      width: "15%",
      float: "left",
      height: "70px"
    },
    wh20: {
        width: "20%",
        float: "left",
        height: "70px"
    },
    wh25: {
      width: "25%",
      float: "left",
      height: "70px"
    },
    wh30: {
      width: "30%",
      float: "left",
      height: "70px"
    },
    wh40: {
      width: "40%",
      float: "left",
      height: "70px"
    },
    w5: {
      width: "5%",
      float: "left",
    },
    w10: {
      width: "10%",
      float: "left",
    },
    w15: {
      width: "15%",
      float: "left",
    },
    w20: {
        width: "20%",
        float: "left",
    },
    w25: {
      width: "25%",
      float: "left",
    },
    w30: {
      width: "30%",
      float: "left",
    },
    w40: {
      width: "40%",
      float: "left",
    },
    row: {
      float: "left",
      width: "100%"
    },
    success: {
      color: 'green'
    },
    danger: {
      color: 'red'
    },
    mgl5: {
      marginLeft: '5px'
    }
}

export default Cards;
