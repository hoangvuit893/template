import React, { Component } from 'react';

import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  Button,
  Modal, ModalHeader, ModalBody, ModalFooter,
  Form,
} from 'reactstrap';
import 'moment-timezone';
import validateInput from '../../shared/validations/news';
import TextArea from "../Common/TextArea";
import TextFieldGroup from "../Common/TextFieldGroup";
import Pagination from "react-js-pagination";
import DateTimePicker from 'react-datetime-picker';
import Moment from 'react-moment';
let headers = new Headers();
const auth = localStorage.getItem('auth');
headers.append('Authorization', 'Bearer ' + auth);
headers.append('Content-Type', 'application/json');

class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
        data: [],
        title_en: '',
        title_jp: '',
        image: '',
        video: '',
        description_en: '',
        description_jp: '',
        deleted: false,
        isLoading: false,
        errors: {},
        action: '',
        created: '',
        activePage: 1,
        numPage: 1,
        itemsCount: 0,
        itemPerPage: 20,
        openDate: new Date()
    };
  }
  async componentDidMount() {
      // this.loadData()
  }
  loadData = () => {
      const page = this.state.activePage || 1;
      const limit = this.state.itemPerPage || 200;
      const fetchData = {
          method: 'GET',
          headers: headers
      };
      fetch(global.BASE_URL + '/news?limit=' + limit + '&page=' + page, fetchData).then( cards => {
          cards.json().then(result => {
              this.setState({
                  data: result.data,
                  itemsCount: result.totalItems,
                  isLoading: false
              });
          })
      }).catch(console.log);
  }
  toggle(action) {
    this.setState({
      modal: !this.state.modal,
      action,
      isLoading: false,
      updateId:'',
      title_en: '',
      title_jp: '',
      image: '',
      video: '',
      description_en: '',
      description_jp: '',
      deleted: false,
      errors: {},
        openDate: new Date()
    });
  }
  execUpdate = (item) => {
      this.setState({
          action: 'update',
          updateId: item._id,
          title_en: item.title_en  || '',
          title_jp: item.title_jp  || '',
          description_en: item.description_en  || '',
          description_jp: item.description_jp  || '',
          image: item.image,
          video: item.video,
          openDate: new Date(item.openDate),
          modal: !this.state.modal,
      });
  }
  cancelCreate() {
    this.setState({
      modal: false,
    });
  }
  updateApp= () => {
    if(this.isValid()) {
      this.setState({ errors: {}, isLoading: true });
      const id = this.state.updateId;
      // process utc time
      // end process utc time
      const body = {
                        title_en: this.state.title_en,
                        title_jp: this.state.title_jp,
                        image: this.state.image,
                        video: this.state.video,
                        description_en: this.state.description_en,
                        description_jp: this.state.description_jp,
                        openDate: this.state.openDate,
                  };
      if (this.state.action === 'update') {
        const fetchData = {
          method: 'PUT',
          headers: headers,
          body: JSON.stringify(body)
        };
        fetch(global.BASE_URL + '/news/'+id, fetchData).then(async () => {
          this.cancelCreate();
          this.loadData();
          this.setState({created: 'News updated successfully', isLoading: false, deleted: false});
        }).catch(console.log);
      } else {
        const fetchData = {
          method: 'POST',
          headers: headers,
          body: JSON.stringify(body)
        };
        console.log(fetchData)
        fetch(global.BASE_URL + '/news', fetchData).then(async () => {
          this.cancelCreate();
          this.loadData();
          this.setState({created: 'News created successfully', isLoading: false, deleted: false});
        }).catch(console.log);
      }
    }
  }
  isValid() {
    const {errors, isValid } = validateInput(this.state);
    if(!isValid) {
      this.setState({errors});
    }
    return isValid;
  }
  inputChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  deleteCard = id => {
    if (window.confirm ('Are you sure to delete this item?')) {
      const fetchData = {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify({deleted: true})
      };
      fetch(global.BASE_URL + '/news/' + id, fetchData).then(() => {
        this.setState({deleted: 'Item deleted', created: ''});
        // reload list
          this.loadData()
      }).catch(console.log);
    }
  }
    handlePageChange = async pageNumber => {
        this.setState({activePage: pageNumber, isLoading: true}, () => {
            this.loadData();
        });
    };
    onDateChange = date => {
        if(date) {
            // date.setMinutes(0);
            // date.setSeconds(0);
            // date.setMilliseconds(0);
            this.setState({ openDate: date });
        }
    }
  render() {
    const {data, title_en, title_jp, description_en, description_jp, image, video, errors, openDate} = this.state;
      const page = this.state.activePage;
      const limit = this.state.itemPerPage;
    if(!this.state.isLoading) {
      return (
        <div className="animated fadeIn">
          <Row>
            <Col>
              <p style={styles.success}>{this.state.created}</p>
              <p style={styles.danger}>{this.state.deleted}</p>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> Brand
                  <Button outline color="primary" style={styles.floatRight} size="sm"  onClick={e=>this.toggle('new')}>Add</Button>
                </CardHeader>
                <CardBody>
                  <Table responsive>
                    <thead>
                    <tr>
                      <th style={styles.wh5}>No.</th>
                      <th style={styles.wh15}>Name</th>
                      <th style={styles.wh10}>Image</th>
                      <th style={styles.wh30}>Description</th>
                      <th style={styles.wh10}>Date</th>
                      <th style={styles.wh15}></th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                      data.map((item, i) => {
                          item.description_en = item.description_en || '';
                          item.description_jp = item.description_jp || '';
                          return (
                            <tr key={i} style={styles.row}>
                              <td style={styles.w5}>{(page-1)*limit+i+1}</td>
                              <td style={styles.w15}>{item.title_en}</td>
                                <td style={styles.w10}><a href={item.image} target="_blank">{item.image ? 'View Image' : ''}</a></td>
                                <td style={styles.w30}>{item.description_en.substr(0,100) + (item.description_en.length > 100 ? '...' : '')}<br/>{item.description_jp.substr(0,100) + (item.description_jp.length > 100 ? '...' : '')}</td>
                                <td style={styles.w10}><Moment  format="YYYY/MM/DD">{item.openDate}</Moment></td>
                              <td style={styles.w15}>
                                  <Button outline color="primary" size="sm" onClick={(e) => this.execUpdate(item) }>Update</Button>&nbsp;
                                  <Button outline color="danger" size="sm" onClick={(e) => this.deleteCard(item._id) }>Delete</Button>
                              </td>
                            </tr>
                          );
                      })
                    }
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
                <Pagination
                    activePage={this.state.activePage}
                    itemsCountPerPage={this.state.itemPerPage}
                    totalItemsCount={this.state.itemsCount}
                    pageRangeDisplayed={10} // so luong item hien thi tren pagination number
                    onChange={e => this.handlePageChange(e)}
                    itemClass="page-item"
                    linkClass="page-link"
                />
            </Col>
          </Row>
          <Modal isOpen={this.state.modal} toggle={e=>this.cancelCreate()} className={this.props.className}>
              <ModalHeader toggle={()=>this.toggle()}>{this.state.action === 'update'? 'Edit' : 'Create '}</ModalHeader>
              <ModalBody>
                <Form>
                {/*<FormGroup>*/}
                    <TextFieldGroup
                        field="title_en"
                        label="Name"
                        value={title_en}
                        error={errors.title_en}
                        onChange={e=>this.inputChange(e)}
                    />
                    <div className="position-relative form-group">
                        <div className="form-group">
                            <label className="control-label">Date</label>
                            <DateTimePicker
                                onChange={this.onDateChange}
                                value={openDate}
                                locale="en-US"
                                disableClock={true}
                                format="y/MM/dd"
                            />
                            <span className="help-block error">{errors.openDate}</span>
                        </div>
                    </div>
                    <TextFieldGroup
                        field="image"
                        label="Image"
                        value={image}
                        error={errors.image||''}
                        onChange={e => this.inputChange(e)}
                    />
                    <TextArea
                        field="description_en"
                        label="Description"
                        value={description_en}
                        error={errors.description_en}
                        onChange={e=>this.inputChange(e)}
                        rows="7"
                    />
                </Form>
              </ModalBody>
              <ModalFooter>
                <Button color="primary" onClick={e=>this.updateApp()} disabled={this.state.isLoading}>Save</Button>&nbsp;&nbsp;
                <Button color="secondary" onClick={e=>this.cancelCreate()}>Cancel</Button>
              </ModalFooter>
            </Modal>
        </div>
      );
    }
    return (
      <div id="page-loading">
        <div className="three-balls">
          <div className="ball ball1"></div>
          <div className="ball ball2"></div>
          <div className="ball ball3"></div>
        </div>
      </div>
    );
  }
}

const styles = {
    a : {
        textDecoration: 'none'
    },
    floatRight: {
      float: "right"
    },
    spinner: {
      width: "30px"
    },
    center: {
      textAlign: "center"
    },
    tbody: {
      height: "380px",
      overflowY: "auto"
    },
    wh5: {
      width: "5%",
      float: "left",
      height: "70px"
    },
    wh10: {
      width: "9.8%",
      float: "left",
      height: "70px"
    },
    wh15: {
      width: "15%",
      float: "left",
      height: "70px"
    },
    wh20: {
        width: "20%",
        float: "left",
        height: "70px"
    },
    wh25: {
      width: "25%",
      float: "left",
      height: "70px"
    },
    wh30: {
      width: "30%",
      float: "left",
      height: "70px"
    },
    wh40: {
      width: "40%",
      float: "left",
      height: "70px"
    },
    w5: {
      width: "5%",
      float: "left",
    },
    w10: {
      width: "10%",
      float: "left",
    },
    w15: {
      width: "15%",
      float: "left",
    },
    w20: {
        width: "20%",
        float: "left",
    },
    w25: {
      width: "25%",
      float: "left",
    },
    w30: {
      width: "30%",
      float: "left",
    },
    w40: {
      width: "40%",
      float: "left",
    },
    row: {
      float: "left",
      width: "100%"
    },
    success: {
      color: 'green'
    },
    danger: {
      color: 'red'
    },
    mgl5: {
      marginLeft: '5px'
    }
}

export default News;
