import React from 'react';
import Loadable from 'react-loadable'
// import DefaultLayout from './containers/DefaultLayout';
function Loading() {
  return <div>Loading...</div>;
}
const User = Loadable({
  loader: () => import('./views/Users'),
  loading: Loading,
});

const Others = Loadable({
  loader: () => import('./views/Others'),
  loading: Loading,
});

const HashTag = Loadable({
    loader: () => import('./views/HashTag'),
    loading: Loading,
});
const Product = Loadable({
    loader: () => import('./views/Product'),
    loading: Loading,
});
const Type = Loadable({
    loader: () => import('./views/Type'),
    loading: Loading,
});
const Color = Loadable({
  loader: () => import('./views/Color'),
  loading: Loading,
});

const Brand = Loadable({
    loader: () => import('./views/Brand'),
    loading: Loading,
});
const WithdrawalInformation = Loadable({
  loader: () => import('./views/WithdrawalInformation'),
  loading: Loading,
});
const Ipfs = Loadable({
    loader: () => import('./views/IPFS'),
    loading: Loading,
});

// const BrowserLinksCategories = Loadable({
//     loader: () => import('./views/CategoryBrowserLink'),
//     loading: Loading,
// });
// const Banner = Loadable({
//     loader: () => import('./views/Banner'),
//     loading: Loading,
// });
// const ReportMessages = Loadable({
//     loader: () => import('./views/ReportMessages'),
//     loading: Loading,
// });
// const NFTBanner = Loadable({
//     loader: () => import('./views/NFTBanner'),
//     loading: Loading,
// });
// const Dashboard = Loadable({
//   loader: () => import('./views/Dashboard'),
//   loading: Loading,
// });

// =========================
// const TwitterUser = Loadable({
//   loader: () => import('./views/TwitterUsers'),
//   loading: Loading
// })
// const TwitterPost = Loadable({
//     loader: () => import('./views/TwitterPost'),
//     loading: Loading
// })
// const Downloads = Loadable({
//   loader: () => import('./views/Downloads'),
//   loading: Loading,
// });
// const UserRanking = Loadable({
//     loader: () => import('./views/Ranking'),
//     loading: Loading,
// });
// const Log = Loadable({
//   loader: () => import('./views/Log/Log'),
//   loading: Loading,
// });
// const Setting = Loadable({
//   loader: () => import('./views/Setting/Setting'),
//   loading: Loading,
// });
const routes = [
  // { path: '/', exact: true, name: 'Home', component: DefaultLayout },
  // { path: '/twitter-users', exact: true, name: 'Twitter Users', component: TwitterUser },
  // { path: '/twitter-posts', exact: true, name: 'Twitter Posts', component: TwitterPost },
  // { path: '/users', exact: true, name: 'Users', component: User },
  // { path: '/ranking', exact: true, name: 'Ranking', component: UserRanking },
  // { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  // { path: '/community/:id', exact: true,  name: 'Community', component: Community },
  // { path: '/log', exact: true,  name: 'Log', component: Log },
  // { path: '/settings', exact: true,  name: 'Settings', component: Setting },
  // { path: '/browser-links-categories', exact: true,  name: 'Browser Links Categories', component: BrowserLinksCategories },
  // { path: '/banners', exact: true,  name: 'Banners', component: Banner },
  // { path: '/tokens', exact: true,  name: 'Browser Links', component: Token },
  // { path: '/downloads', exact: true,  name: 'Downloads', component: Downloads },
  // { path: '/reports', exact: true,  name: 'Reports', component: ReportMessages },
  // { path: '/nft-banner', exact: true,  name: 'Reports', component: NFTBanner },
  // { path: '/other-settings', exact: true,  name: 'Others Settings', component: Others},
  // { path: '/withdrawal-informations', exact: true, name: 'Withdrawal Informations', component: WithdrawalInformation},
  // { path: '/ipfs', exact: true,  name: 'Ipfs', component: Ipfs},
  // { path: '/tags', exact: true,  name: 'Hash Tags', component: HashTag},
  { path: '/product', exact: true, name: 'Product', component: Product },
  { path: '/type', exact: true, name: 'Type', component: Type },
  { path: '/color', exact: true, name: 'Color', component: Color },
  { path: '/brand', exact: true, name: 'Brand', component: Brand },
];

export default routes;
