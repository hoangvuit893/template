export default {
  items: [
    // {
    //   name: 'Dashboard',
    //   url: '/dashboard',
    //   icon: 'icon-speedometer',
    //   badge: {
    //     variant: 'info',
    //     text: 'NEW',
    //   },
    // },
    {
      title: true,
      name: 'Menu',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
      {
          name: 'Users ユーザー',
          url: '/users',
          icon: 'icon-user'
      },
    {
        name: 'Brand',
        url: '/brand',
        icon: 'fa fa-newspaper-o',
    },
    {
      name: 'Product',
      url: '/product',
      icon: 'icon-minus',
    },
    {
      name: 'Type',
      url: '/type',
      icon: 'icon-minus',
    },
    {
      name: 'Color',
      url: '/color',
      icon: 'icon-minus',
    },
    // {
    //   name: 'Other Settings 作成できるアドレスリスト',
    //   url: '/other-settings',
    //   icon: 'icon-minus',
    //   // forRoles: ['admin']
    // },
    // {
    //   name: 'Withdrawal Informations',
    //   url: '/withdrawal-informations',
    //   icon: 'icon-star',
    // },
    // {
    //     name: 'IPFS',
    //     url: '/ipfs',
    //     icon: 'icon-minus',
    //     forRoles: ['admin']
    // },
    // {
    //   name: 'TAGS',
    //   url: '/tags',
    //   icon: 'icon-minus',
    // },

  ],
};
