import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export default function validateinput(data) {
    let errors = {};

    if(Validator.isEmpty(data.title_jp)) {
        errors.title_jp = 'Please enter Japanese title'
    }
    // if(Validator.isEmpty(data.title_en)) {
    //     errors.title_en = 'Please enter English title'
    // }
    if(Validator.isEmpty(data.description_en)) {
        errors.description_en = 'Please enter English description'
    }
    if(Validator.isEmpty(data.description_jp)) {
        errors.description_jp = 'Please enter Japanese description'
    }
    if(Validator.isEmpty(data.openDate.toString())) {
        errors.openDate = 'Please enter date'
    }
    return {
        errors,
        isValid: isEmpty(errors)
    }
}